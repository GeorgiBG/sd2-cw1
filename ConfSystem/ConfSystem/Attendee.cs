﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class Attendee:
    * - Public class describing an Attendee's specific attributes as stated in the system requirements,
    * such as Attendee Ref, Conference Name, Registration Type, Paid, Presenter and Paper Title for presenters,
    * and also takes institution as an object of type Institution that has Institution Name and Address stored inside the class.
    * - Inherits from the Person abstract class receiving the First Name and Last Name (validated in the class),
    * as well as the base constructor(s) that set and validate those two fields.
    * - Validates all the fields in their getters and setters, declares and defines(implements) a method 'getCost',
    * which calculates the attendance cost to the conference for every attendee individually, depending on registration details.
    * - Having the basic non-paramatarised and parametrised constructors implementing the base ones (from Person),
    * and extending them to handle the rest of the fields specific to this class.
    
    * Description of class AttendeeEventArgs:
    * - Public class inheriting from the EventArgs class(containing event data), to add custom arguments to the event handler.
    * - The data will be processed and raised in the event handler to achieve a connection bridge 
    * between the Attendee class and the Front-end (InvoiceWindow and CertificateWindow).
    * - You will be able to retrieve all the specified arguments in the class containing the data of the original
    * Attendee class coresponding fields.
    
    * Last modified: 10/26/2016
*/

using System;

namespace ConfSystem
{
    public class AttendeeEventArgs : EventArgs
    {
        // public getter/setter property for FirstName event argument
        public string FirstName { get; set; }
        // Public getter/setter property for LastName event argument
        public string LastName { get; set; }
        // Public getter/setter property for Cost event argument
        public decimal Cost { get; set; }
        // Public getter/setter property for InstitutionName event argument
        public string InstitutionName { get; set; }
        // Public getter/setter property for InstitutionAddress event argument
        public string InstitutionAddress { get; set; }
        // Public getter/setter property for ConferenceName event argument
        public string ConferenceName { get; set; }
        // Public getter/setter property for Presenter event argument
        public bool Presenter { get; set; }
        // Public getter/setter property for PaperTitle event argument
        public string PaperTitle { get; set; }
    }

    public class Attendee : Person
    {
        #region class variable members
        /* Private  members */
        // private field for Attendee's attendee ref attribute
        private ushort attendeeRef;
        // private field for Attendee's conference name attribute
        private string conferenceName;
        // private field for Attendee's registration type attribute
        private string registrationType;
        // private field for Attendee's paid attribute
        private bool paid;
        // private field for Attendee's presenter attribute
        private bool presenter;
        // private field for Attendee's paper title attribute
        private string paperTitle;

        /* Public members */
        // public field for the Institution's object
        public Institution institution;
        #endregion

        #region Getters and Setters
        // Public property allowing to modify the 'attendeeRef' private field by adding validation to it.
        public ushort AttendeeRef
        {
            get { return attendeeRef; }
            protected set
            {
                // validate the value
                if (value < 40000 || value > 60000)
                    throw new ArgumentException("Attendee ref::The data is out of range! Must be between 40000 and 60000.");
                // set the validated value
                attendeeRef = value;
            }
        }

        // Public property allowing to modify the 'conferenceName' private field by adding validation to it.
        public string ConferenceName
        {
            get { return conferenceName; }
            protected set
            {
                // validate the value
                if (value == string.Empty)
                    throw new ArgumentException("Conference name::The field cannot be empty! Must enter at least one character.");
                // set the validated value
                conferenceName = value;
            }
        }

        // Public property allowing to modify the 'registrationType' private field by adding validation to it.
        public string RegistrationType
        {
            get { return registrationType; }
            protected set
            {
                // validate the value
                if (value == "Full" || value == "Student" || value == "Organiser")
                {
                    // set the validated value
                    registrationType = value;
                }
                else
                {
                    // error exception throw
                    throw new ArgumentException("Registration type::The data is incorrect! Can only be set to \"Full\", \"Student\" or \"Organiser\".");
                }
            }
        }

        // Public property allowing to modify the 'paid' private field.
        public bool Paid
        {
            get { return paid; }
            protected set { paid = value; }
        }

        // Public property allowing to modify the 'presenter' private field.
        public bool Presenter
        {
            get { return presenter; }
            protected set { presenter = value; }
        }

        // Public property allowing to modify the 'paperTitle' private field by adding validation to it.
        public string PaperTitle
        {
            get { return paperTitle; }
            protected set
            {
                // validate and set
                if (Presenter)
                    paperTitle = value;
                else
                    paperTitle = string.Empty;
            }
        }
        #endregion

        #region Delegates and Events
        // Event handler for the Attendee class data
        public event EventHandler<AttendeeEventArgs> AttendeeDataDemanded;
        
        // Processing the data communicated via the event handler
        public void processAttendeeData(
            string firstName, string lastName, decimal cost,
            string institutionName, string institutionAddress, string conferenceName, bool presenter, string paperTitle)
        {
            OnAttendeeDataDemanded(firstName, lastName, cost, institutionName, institutionAddress, conferenceName, presenter, paperTitle);
        }
        
        // Raising the event
        protected virtual void OnAttendeeDataDemanded(
            string firstName, string lastName, decimal cost,
            string institutionName, string institutionAddress, string conferenceName, bool presenter, string paperTitle)
        {
            AttendeeDataDemanded?.Invoke(this, new AttendeeEventArgs() {
                FirstName = firstName,
                LastName = lastName,
                Cost = cost,
                InstitutionName = institutionName,
                InstitutionAddress = institutionAddress,
                ConferenceName = conferenceName,
                Presenter = presenter,
                PaperTitle = paperTitle
            });
        }
        #endregion

        #region Constructors and Destructors
        // Empty/Non-parametarised constructor
        public Attendee() : base() { }
        // Parametarised constructor, setting the Attendee's(including the inhertied from Person) properties 
        // and taking an 'Action' method that is going to be responsible for displaying any validation messages concerning this class.
        public Attendee(string firstName, string lastName, ushort attendeeRef, string conferenceName, 
            string registrationType, bool paid, bool presenter, string paperTitle, 
            string institutionName, string institutionAddress, Action validationMessage)
            : base(firstName, lastName, validationMessage)
        {
            try
            {
                AttendeeRef = attendeeRef;
            }
            catch(Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }

            try
            {
                ConferenceName = conferenceName;
            }
            catch(Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }

            try
            {
                RegistrationType = registrationType;
            }
            catch (Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }

            Paid = paid;
            Presenter = presenter;

            try
            {
                PaperTitle = paperTitle;
            }
            catch (Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }
            
            // create the institution
            institution = new Institution(institutionName, institutionAddress);
            
            // if successfully created, set the message to created
            if (institution != null)
            {
                MessageHolder.message = "Created";
                validationMessage();
                return;
            }
        }
        
        // Destructor
        ~Attendee() { }
        #endregion

        #region Public methods
        // Calculate the price for attendance of the conference, depending on the Attendee's RegistrationType
        public decimal getCost()
        {
            decimal cost = 0;

            if(RegistrationType != string.Empty)
            {
                // Define attendee role
                switch(RegistrationType)
                {
                    case "Full":
                        cost = 500;
                        break;
                    case "Student":
                        cost = 300;
                        break;
                    case "Organiser":
                        cost = 0;
                        break;
                    default:
                        break;
                }
                // Apply discount
                if (Presenter && RegistrationType != "Organiser")
                {
                    float discountPercentage = 0.1f;
                    var discountedPrice = cost * ((decimal)discountPercentage);
                    cost -= discountedPrice;
                }
            }

            return cost;
        }
        #endregion
    }
}