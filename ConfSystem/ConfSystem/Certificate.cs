﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class Institution:
    * - Public class responsible for the data transfer between Attendee class (back-end) and Certificate Window (front-end)
    * - Populates the window with information for the following fields:
    * FirstName, LastName, ConferenceName, and if the attendee is presenter, also with PaperTitle.

    * Last modified: 10/26/2016
*/

using System;
using System.Windows;

namespace ConfSystem
{
    public class Certificate
    {
        // Send Attendee attributes data to Certificate Window, populate the required Controls and Display CertificateWindow
        public void OnAttendeeDataDemanded(object source, AttendeeEventArgs attendee)
        {
            var certificateWindow = new CertificateWindow();

            certificateWindow.attendeeNameLbl.Content = attendee.FirstName + " " + attendee.LastName;
            certificateWindow.attendeeConferenceLbl.Content = attendee.ConferenceName;
            certificateWindow.certificateDateLbl.Content = DateTime.Now.ToString();
            
            if (attendee.Presenter)
            {
                certificateWindow.attendeePaperTitleLbl.Content = attendee.PaperTitle;
                certificateWindow.presentedLbl.Visibility = Visibility.Visible;
                certificateWindow.attendeePaperTitleLbl.Visibility = Visibility.Visible;
            }

            certificateWindow.ShowDialog();
        }
    }
}
