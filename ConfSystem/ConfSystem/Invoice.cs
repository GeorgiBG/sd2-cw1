﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class Institution:
    * - Public class responsible for the data transfer between Attendee class (back-end) and Invoice Window (front-end)
    * - Populates the window with information for the following fields:
    * FirstName, LastName, InstitutionName, InstitutionAddress, ConferenceName and Cost(from getCost method).

    * Last modified: 10/26/2016
*/

using System;
namespace ConfSystem
{
    public class Invoice
    {
        // Send Attendee attributes data to Invoice Window, populate the required Controls and Display InvoiceWindow
        public void OnAttendeeDataDemanded(object source, AttendeeEventArgs attendee)
        {
            var invoiceWindow  = new InvoiceWindow();

            invoiceWindow.attendeeNameLbl.Content = attendee.FirstName + " " + attendee.LastName;
            invoiceWindow.attendeeInstituteLbl.Content = attendee.InstitutionName;
            invoiceWindow.attendeeInstituteAddrLbl.Content = attendee.InstitutionAddress;
            invoiceWindow.attendeeConfereneLbl.Content = attendee.ConferenceName;
            invoiceWindow.attendeeCostLbl.Content = "£" + ((int)attendee.Cost).ToString();
            invoiceWindow.invoiceDateLbl.Content = DateTime.Now.ToString();

            invoiceWindow.ShowDialog();
        }
    }
}
