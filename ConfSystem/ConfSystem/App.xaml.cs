﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ConfSystem
{
    public partial class Application : System.Windows.Application
    {
        // Uri uri = new Uri("/Images/icon.png", UriKind.Relative);
        // Uri uri = new Uri("pack://application:,,,/Images/icon.png", UriKind.Absolute);

        public string DisplayedImage
        {
            get
            {
                return "/ConfSystem;component/Images/icon.png";
            }
        }
    }
}
