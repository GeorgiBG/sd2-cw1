﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class Person:
    * - Abstract class describing a general Person's attributes, at least those that are in the context of the system requirements,
    * such as First Name and Last Name.
    * - It is abstract because the system does not need to instantiate a simple Person and it is going
    * to be inherited from an Attendee class - expanding the properties, methods and functionality.
    * - Having the basic non-paramatarised and parametrised constructors, being declared as protected, 
    * will set a base implementation that will be extended from.
    
    * Description of class MessageHolder:
    * - Static class that is used to store the state of the validated fileds/properties.
    * - Only one static field - message of the type string will be needed at this point.
    
    * Last modified: 10/26/2016
*/

using System;

namespace ConfSystem
{
    public static class MessageHolder
    {
        // Static string field holding the Validation Message of the Form on MainWindow
        public static string message;
    }

    public abstract class Person
    {
        #region class variable members
        // private field for Person's first name attribute
        private string firstName;
        // private field for Person's last name attribute
        private string lastName;
        #endregion

        #region Getters and Setters
        // Public property allowing to modify the 'firstName' private field by adding validation to it.
        public string FirstName
        {
            get { return firstName; }
            protected set
            {
                // validate the value
                if (value == string.Empty)
                    throw new ArgumentException("First name::The field cannot be empty! Must enter at least one character.");
                // set the validated value
                firstName = value;
            }
        }

        // Public property allowing to modify the 'lastName' private field by adding validation to it.
        public string LastName
        {
            get { return lastName; }
            protected set
            {
                // validate the value
                if (value == string.Empty)
                    throw new ArgumentException("Last name::The field cannot be empty! Must enter at least one character.");
                // set the validated value
                lastName = value;
            }
        }
        #endregion

        #region Constructors and Destructors
        // Empty/Non-parametarised constructor
        protected Person() { }
        // Parametarised constructor, setting the Person's properties and taking an 'Action' method
        // that is going to be responsible for displaying any validation messages concerning this class.
        protected Person(string firstName, string lastName, Action validationMessage)
        {
            // Trying to set the 'FirstName' property's value, 
            // otherwise call the 'validationMessage' method with an exception message assigned to it.
            try
            {
                FirstName = firstName;
            }
            catch (Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }

            // Trying to set the 'LastName' property's value, 
            // otherwise call the 'validationMessage' method with an exception message assigned to it.
            try
            {
                LastName = lastName;
            }
            catch (Exception excep)
            {
                MessageHolder.message = excep.Message;
                validationMessage();
                return;
            }
        }

        // Destructor
        ~Person() { }
        #endregion
    }
}
