﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class MainWindow:
    * - The class provides the front-end functionality of the Main window
    
    * Last modified: 10/26/2016
*/

using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ConfSystem
{
    public partial class MainWindow
    {
        // object of type Attendee(class), will be used to create Attendee instances with populated fields in the system.
        private Attendee attendee;

        // Set initially test/history board data insertion allowance
        private bool canPrintToBoard = false;

        // Initialize the window
        public MainWindow()
        {
            InitializeComponent();
        }

        // Create attendee object and clear the input form + log board printing
        private void setBtn_Click(object sender, RoutedEventArgs e)
        {
            CreateAttendee();
            PrintTestBoard();
            ClearInputFields(this);
        }

        // Clear the input form
        private void clearBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearInputFields(this);
        }

        // Populate the Attendee object fields
        private void getBtn_Click(object sender, RoutedEventArgs e)
        {
            GetAttendeeData();
        }

        // Open a new window with Attendee's name, institution and attendance cost
        private void invoiceBtn_Click(object sender, RoutedEventArgs e)
        {
            createInvoice(new Invoice());
        }

        // Open a new window with Attendee's personal certificate including his details
        private void certificateBtn_Click(object sender, RoutedEventArgs e)
        {
            createCertificate(new Certificate());
        }
        
        // Clears the Test Board displaying the added attendees details history
        private void clearBoardBtn_Click(object sender, RoutedEventArgs e)
        {
            CleanTestBoard();
        }
    }

    /*
     * A seperate partial class to store the helper methods,
     * so the former one stays a bit leaner and to the point.
    */
    public partial class MainWindow
    {
        #region Helper Methods
        // Helper to create the Invoice Window, taking Invoice class obj. as paramater
        private void createInvoice(Invoice invoice)
        {
            if (attendee != null)
            {
                if (MessageHolder.message != string.Empty && MessageHolder.message != "Created")
                {
                    MessageBox.Show(MessageHolder.message);
                    return;
                }

                // Subscribe AttendeeDataDemanded for Invoice
                attendee.AttendeeDataDemanded += invoice.OnAttendeeDataDemanded;
                attendee.processAttendeeData(
                    attendee.FirstName, attendee.LastName, attendee.getCost(),
                    attendee.institution.InstitutionName, attendee.institution.InstitutionAddress,
                    attendee.ConferenceName, attendee.Presenter, attendee.PaperTitle
                );
                // Unsubscribe AttendeeDataDemanded for Invoice
                attendee.AttendeeDataDemanded -= invoice.OnAttendeeDataDemanded;
            }
            else
                MessageBox.Show("Do not forget to first add an Attendee in order to display an Invoice.");
        }

        // Helper to create the Certificate Window, taking Certificate class obj. as paramater
        private void createCertificate(Certificate certificate)
        {
            if (attendee != null)
            {
                if (MessageHolder.message != string.Empty && MessageHolder.message != "Created")
                {
                    MessageBox.Show(MessageHolder.message);
                    return;
                }

                // Subscribe AttendeeDataDemanded for Certificate
                attendee.AttendeeDataDemanded += certificate.OnAttendeeDataDemanded;
                attendee.processAttendeeData(
                    attendee.FirstName, attendee.LastName, attendee.getCost(),
                    attendee.institution.InstitutionName, attendee.institution.InstitutionAddress,
                    attendee.ConferenceName, attendee.Presenter, attendee.PaperTitle
                );
                // Unsubscribe AttendeeDataDemanded for Certificate
                attendee.AttendeeDataDemanded -= certificate.OnAttendeeDataDemanded;
            }
            else
                MessageBox.Show("Do not forget to firstly add an Attendee in order to create a Certificate.");
        }

        // Helper to display the data stored in the Attendee's obj. attributes to the corresponding Controls/Fields
        private void GetAttendeeData()
        {
            if(attendee != null && attendee.institution != null)
            {
                attendeeRefTxt.Text = attendee.AttendeeRef.ToString();
                firstNameTxt.Text = attendee.FirstName.ToString();
                lastNameTxt.Text = attendee.LastName.ToString();
                institutionNameTxt.Text = attendee.institution.InstitutionName.ToString();
                institutionAddressTxt.Text = attendee.institution.InstitutionAddress.ToString();
                conferenceNameTxt.Text = attendee.ConferenceName.ToString();
                registrationTypeCombo.SelectedValuePath = "Content";
                registrationTypeCombo.SelectedValue = attendee.RegistrationType.ToString();
                paidCheck.IsChecked = attendee.Paid;
                presenterCheck.IsChecked = attendee.Presenter;
                paperTitleTxt.Text = attendee.PaperTitle.ToString();
            }
            else
                MessageBox.Show("There is no existing data to be fetched.");
        }

        // Helper to create an Attendee instance, populating it with the data passed in the Window's Controls/Fields
        private void CreateAttendee()
        {
            ushort attendeeRef;
            bool attendeeRefInt = ushort.TryParse(attendeeRefTxt.Text, NumberStyles.Integer, CultureInfo.InvariantCulture, out attendeeRef);                
            string firstName = firstNameTxt.Text;
            string lastName = lastNameTxt.Text;
            string institutionName = institutionNameTxt.Text;
            string institutionAddress = institutionAddressTxt.Text;
            string conferenceName = conferenceNameTxt.Text;

            string registrationType;
            if ((ComboBoxItem)registrationTypeCombo.SelectedItem != null)
            {
                var comboItem = (ComboBoxItem)registrationTypeCombo.SelectedItem;
                registrationType = comboItem.Content.ToString();
            }
            else
                registrationType = string.Empty;

            bool paid = (bool)paidCheck.IsChecked;
            bool presenter = (bool)presenterCheck.IsChecked;
            string paperTitle = paperTitleTxt.Text;

            attendee = new Attendee
            (
                firstName, lastName, attendeeRef, conferenceName,
                registrationType, paid, presenter, paperTitle,
                institutionName, institutionAddress,
                (() => DisplayValidationMessage())
            );

            if (MessageHolder.message == "Created")
                canPrintToBoard = true;
        }

        // Helper to clear the Controls
        void ClearInputFields(DependencyObject obj)
        {
            TextBox textBox = obj as TextBox;
            CheckBox checkBox = obj as CheckBox;
            ComboBox comboBox = obj as ComboBox;

            if (textBox != null)
                textBox.Text = string.Empty;

            if (checkBox != null)
                checkBox.IsChecked = false;

            if (comboBox != null)
                comboBox.SelectedIndex = -1;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj as DependencyObject); i++)
                ClearInputFields(VisualTreeHelper.GetChild(obj, i));
        }

        // Helper to display the existing messages from the Attendee's attributes validation
        // will be passed in the Attendee's constructor like: () => MethodName()
        private static void DisplayValidationMessage()
        {
            if(MessageHolder.message != string.Empty)
                MessageBox.Show(MessageHolder.message);
        }

        // Helper to Add to a ListBox that I use as a History board, displaying all added Attendee's details
        private void PrintTestBoard()
        {
            if (attendee != null && canPrintToBoard)
            {
                if (testBoard.Items.Count > 0)
                    testBoard.Items.Add(string.Concat(Enumerable.Repeat("-", 99)));

                testBoard.Items.Add(string.Join(": ", "Attendee Ref", attendee.AttendeeRef));
                testBoard.Items.Add(string.Join(": ", "First Name", attendee.FirstName));
                testBoard.Items.Add(string.Join(": ", "Last Name", attendee.LastName));
                testBoard.Items.Add(string.Join(": ", "Institution Name", attendee.institution.InstitutionName));
                testBoard.Items.Add(string.Join(": ", "Institution Address", attendee.institution.InstitutionAddress));
                testBoard.Items.Add(string.Join(": ", "Conference Name", attendee.ConferenceName));
                testBoard.Items.Add(string.Join(": ", "Registration Type", attendee.RegistrationType));
                testBoard.Items.Add(string.Join(": ", "Paid", attendee.Paid ? "Yes" : "No"));
                testBoard.Items.Add(string.Join(": ", "Presenter", attendee.Presenter ? "Yes" : "No"));
                testBoard.Items.Add(string.Join(": ", "Paper Title", attendee.Presenter ? attendee.PaperTitle : "No paper title - You are not a presenter."));
                testBoard.Items.Add(string.Join(": ", "Cost ", attendee.getCost()));

                // reset data insertion allowance
                canPrintToBoard = false;
            }
        }

        // Helper to Clear the ListBox that I use as a History board
        private void CleanTestBoard()
        {
            if(testBoard.Items.Count > 0)
                testBoard.Items.Clear();
        }
        #endregion
    }
}