﻿/* 
    * Author: Georgi Mirazchiyski
    
    * Description of class Institution:
    * - Public class describing an Institution's attributes, such as Institution Name and Institution Address.
    * - It is used as a component object in Attendee class to specifiy the attendee's institution details.
    * - The declared properties have getters and setters and are set in the parametarised constructor.

    * Last modified: 10/28/2016
*/

namespace ConfSystem
{
    public class Institution
    {
        #region Getters and Setters (variables)
        // Public getter/setter property for InstitutionName.
        public string InstitutionName { get; set; }
        // Public getter/setter property for InstitutionAddress.
        public string InstitutionAddress { get; set; }
        #endregion

        #region Constructors and Destructors
        // Parametarised constructor
        public Institution(string institutionName, string institutionAddress)
        {
            this.InstitutionName = institutionName;
            this.InstitutionAddress = institutionAddress;
        }
        // Destructor
        ~Institution() { }
        #endregion
    }
}
